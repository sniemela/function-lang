package org.simo.function.benchmarks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.simo.function.benchmarks.util.ProductFamilyData;
import org.simo.function.integration.DefaultObjectCallSite;
import org.simo.function.integration.model.ProductFamily;
import org.simo.function.integration.model.ProductFamilyXmlWriter;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionRegistry;
import org.simo.function.runtime.FunctionValuePool;

public class BaseBenchmark {

    protected Environment env;
    protected byte[] productFamilyXmlData;
    protected ProductFamily productFamily;
    protected Transformer transformer;

    protected void setup(String xsltTemplateFile) {
        productFamily = ProductFamilyData.createExampleProductFamily();
        productFamilyXmlData = getProductFamilyData(productFamily);
        env = Environment.create(new FunctionRegistry(), new FunctionValuePool());
        env.setObjectCallSite(new DefaultObjectCallSite());
        transformer = createTransfomer(xsltTemplateFile);
    }

    private Transformer createTransfomer(String xsltTemplateFile) {
        TransformerFactory factory = TransformerFactory.newInstance();
        try {
            Templates xsltTemplate = factory
                    .newTemplates(new StreamSource(SumOfDivisibleByTwoBenchmark.class.getResourceAsStream(xsltTemplateFile)));
            return xsltTemplate.newTransformer();
        } catch (TransformerConfigurationException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    protected byte[] getProductFamilyData(ProductFamily family) {
        ProductFamilyXmlWriter xmlWriter = new ProductFamilyXmlWriter(family);
        byte[] data = new byte[0];
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            outputStream.write("<?xml version='1.0'?>\n".getBytes());
            xmlWriter.write(outputStream);
            data = outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
