package org.simo.function.benchmarks;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QuadraticEquationTest {

    private QuadraticEquationBenchmark benchmark;

    @Before
    public void setup() {
        benchmark = new QuadraticEquationBenchmark();
        benchmark.setup();
    }

    @Test
    public void functionCalculation() {
        String result = benchmark.quadraticEquationFunctionLang();
        Assert.assertEquals("0.25 -1.0", result);
    }

    @Test
    public void javaCalculation() {
        String result = benchmark.quadraticEquationJava();
        Assert.assertEquals("0.25 -1.0", result);
    }

    @Test
    public void xsltCalculation() {
        String result = benchmark.quadraticEquationXSLT();
        Assert.assertEquals("0.25 -1", result);
    }
}
