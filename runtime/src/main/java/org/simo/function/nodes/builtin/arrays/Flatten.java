/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.builtin.arrays;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.simo.function.nodes.FnCallable;
import org.simo.function.nodes.Node;
import org.simo.function.nodes.annotation.BuiltinFunction;
import org.simo.function.nodes.util.NodeUtils;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
@BuiltinFunction
public class Flatten implements FnCallable {

    @Override
    public FunctionValue call(Environment env, Node[] arguments) {
        return env.fv().ofObjectList(flatten(env, NodeUtils.eval(env, arguments), new ArrayList<>()));
    }

    private List<Object> flatten(Environment env, FunctionValue[] values, List<Object> flatList) {
        for (FunctionValue value : values) {
            Object obj = value.asObject();
            env.rfv(value);
            flatten(obj, flatList);
        }
        return flatList;
    }

    private void flatten(Object value, List<Object> flatList) {
        if (value instanceof Collection) {
            Collection<?> collection = (Collection<?>) value;
            for (Object collectionObj : collection) {
                flatten(collectionObj, flatList);
            }
        } else {
            flatList.add(value);
        }
    }
}
