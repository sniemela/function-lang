/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.builtin.arrays;

import java.util.ArrayList;
import java.util.List;

import org.simo.function.nodes.FnCallable;
import org.simo.function.nodes.Node;
import org.simo.function.nodes.annotation.BuiltinFunction;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
@BuiltinFunction(minArgs = 2, maxArgs = 2)
public class Append implements FnCallable {

    @Override
    public FunctionValue call(Environment env, Node[] arguments) {
        FunctionValue listFv = arguments[0].eval(env);
        List<? extends Object> list = listFv.asList();
        env.rfv(listFv);

        List<Object> copy = new ArrayList<>(list);
        FunctionValue value = arguments[1].eval(env);

        if (value.isList()) {
            copy.addAll(value.asList());
        } else {
            copy.add(value.asObject());
        }

        env.rfv(value);

        return env.fv().ofObjectList(copy);
    }
}
