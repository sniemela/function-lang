/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import org.simo.function.nodes.FnCallable;
import org.simo.function.nodes.Function;
import org.simo.function.nodes.Node;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;
import org.simo.function.runtime.exceptions.UndefinedFunctionException;

/**
 * @author sniemela
 *
 */
public final class DirectFunctionCallNode extends Node {
    private final String functionName;
    private final Node[] arguments;
    private FnCallable callable;

    public DirectFunctionCallNode(String functionName, Node[] arguments) {
        this.functionName = functionName;
        this.arguments = arguments;
    }

    @Override
    public FunctionValue eval(Environment env) {
        if (callable != null) {
            return callable.call(env, arguments);
        }

        Function function = env.getFunctionRegistry().lookup(functionName);
        boolean storeCallableRef = true;

        if (function == null) {
            // Fall back to slow path (indirect calls).
            // This node will never recover.
            Object symbol = env.getSymbol(functionName);

            if (!(symbol instanceof Function)) {
                throw new UndefinedFunctionException(functionName);
            }

            function = (Function) symbol;
            storeCallableRef = false;
        }

        ensureArgumentCapacity(function);
        FnCallable fnCallable = function.getCallable();

        if (storeCallableRef) {
            callable = fnCallable;
        }

        return fnCallable.call(env, arguments);
    }

    private void ensureArgumentCapacity(Function fn) {
        int argCount = arguments.length;
        if (argCount < fn.getMinArguments() || argCount > fn.getMaxArguments()) {
            throw new IndexOutOfBoundsException(
                    String.format("Invalid number of arguments (%s) in function '%s', min %s and max %s", argCount,
                            fn.getName(), fn.getMinArguments(), fn.getMaxArguments()));
        }
    }
}
