/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.builtin.arrays;

import java.util.Collection;

import org.simo.function.nodes.FnCallable;
import org.simo.function.nodes.Node;
import org.simo.function.nodes.annotation.BuiltinFunction;
import org.simo.function.nodes.util.NodeUtils;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
@BuiltinFunction(minArgs = 2)
public class Join implements FnCallable {

    @Override
    public FunctionValue call(Environment env, Node[] arguments) {
        FunctionValue[] values = NodeUtils.eval(env, arguments);
        String delim = values[0].asString();
        env.rfv(values[0]);
        return env.fv().ofString(join(env, values, delim, 1));
    }

    public static final String join(Environment env, FunctionValue[] values, String delim) {
        return join(env, values, delim, 0);
    }

    private static final String join(Environment env, FunctionValue[] values, String delim, int startIndex) {
        StringBuilder sb = new StringBuilder(values.length * 16);
        for (int i = startIndex; i < values.length; ++i) {
            FunctionValue fv = values[i];
            sb.append(fv.asObject());

            env.rfv(fv);

            if (i + 1 < values.length) {
                sb.append(delim);
            }
        }
        return sb.toString();
    }

    public static final String join(Collection<?> values, String delim) {
        StringBuilder sb = new StringBuilder(values.size() * 16);
        int size = values.size();
        int index = 0;
        for (Object value : values) {
            sb.append(value);

            if (index + 1 < size) {
                sb.append(delim);
            }

            index++;
        }
        return sb.toString();
    }

    public static final String join(Collection<?> values, char delim) {
        return join(values, String.valueOf(delim));
    }

    public static final String join(Environment env, FunctionValue[] values, char delim) {
        return join(env, values, String.valueOf(delim));
    }
}
