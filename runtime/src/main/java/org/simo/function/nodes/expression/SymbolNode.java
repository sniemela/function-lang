/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import org.simo.function.nodes.Node;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;
import org.simo.function.runtime.exceptions.UndefinedSymbolException;

/**
 * @author Simo
 *
 */
public final class SymbolNode extends Node {
    private final String id;

    public SymbolNode(String id) {
        this.id = id;
    }

    @Override
    public FunctionValue eval(Environment env) {
        Object symbol = env.getSymbol(id);

        if (symbol == null) {
            symbol = env.getFunctionRegistry().lookup(id);
        }

        if (symbol == null) {
            throw new UndefinedSymbolException("Undefined symbol/function: " + id);
        }

        return env.fv().ofObject(symbol);
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id;
    }
}
