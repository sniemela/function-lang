/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression.arithm;

import java.util.List;

import org.simo.function.nodes.Node;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.FunctionValue;

/**
 * @author Simo
 *
 */
public final class MulNode extends Node {
    private final Node left;
    private final Node right;

    public MulNode(Node left, Node right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public FunctionValue eval(Environment env) {
        FunctionValue l = left.eval(env);
        FunctionValue r = right.eval(env);

        if (l.isList() && r.isList()) {
            List<? extends Object> leftList = l.asList();
            List<? extends Object> rightList = r.asList();
            env.rfv(l);
            env.rfv(r);
            return env.fv().ofDouble(vectorMul(env, leftList, rightList));
        }

        double leftDouble = l.asDouble();
        double rightDouble = r.asDouble();
        env.rfv(l);
        env.rfv(r);

        return env.fv().ofDouble(leftDouble * rightDouble);
    }

    private double vectorMul(Environment env, List<? extends Object> l, List<? extends Object> r) {
        if (l.size() != r.size()) {
            throw new IllegalArgumentException("Vector multiplication expects two equal-length arrays");
        }
        double result = 0;
        for (int i = 0; i < l.size(); i++) {
            FunctionValue leftFv = env.fv().ofObject(l.get(i));
            FunctionValue rightFv = env.fv().ofObject(r.get(i));
            double leftDouble = leftFv.asDouble();
            double rightDouble = rightFv.asDouble();
            env.rfv(leftFv);
            env.rfv(rightFv);
            result += leftDouble * rightDouble;
        }
        return result;
    }
}
