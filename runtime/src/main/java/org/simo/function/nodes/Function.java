/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes;

/**
 * @author sniemela
 *
 */
public final class Function {
    private final String name;
    private FnCallable callable;
    private final int minArguments;
    private final int maxArguments;

    public Function(String name, int minArguments, int maxArguments) {
        this.name = name;
        this.minArguments = minArguments;
        this.maxArguments = maxArguments;
    }

    public String getName() {
        return name;
    }

    public void setCallable(FnCallable callable) {
        this.callable = callable;
    }

    public FnCallable getCallable() {
        return callable;
    }

    public int getMaxArguments() {
        return maxArguments;
    }

    public int getMinArguments() {
        return minArguments;
    }
}
