/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.runtime;

import java.util.List;

import org.simo.function.nodes.Function;
import org.simo.function.nodes.builtin.arrays.Join;
import org.simo.function.nodes.util.NumberUtils;
import org.simo.function.runtime.exceptions.FunctionValueException;

public final class FunctionValue {

    private ValueType valueType;

    private String stringValue;
    private double doubleValue;
    private int intValue;
    private boolean booleanValue;
    private List<Double> doubleList;
    private List<String> stringList;
    private Function function;
    private List<? extends Object> objectList;
    private Object object;

    FunctionValue() {
        // Do nothing
    }

    public void reset() {
        doubleValue = 0;
        valueType = null;
        object = null;
        stringValue = null;
        intValue = 0;
        booleanValue = false;
        doubleList = null;
        stringList = null;
        function = null;
        objectList = null;
    }

    public FunctionValue ofDouble(double value) {
        doubleValue = value;
        valueType = ValueType.DOUBLE;
        return this;
    }

    public FunctionValue ofString(String value) {
        stringValue = value;
        valueType = ValueType.STRING;
        return this;
    }

    public FunctionValue ofBoolean(boolean value) {
        booleanValue = value;
        valueType = ValueType.BOOLEAN;
        return this;
    }

    public FunctionValue ofDoubleList(List<Double> doubleList) {
        this.doubleList = doubleList;
        this.valueType = ValueType.DOUBLE_LIST;
        return this;
    }

    public FunctionValue ofStringList(List<String> stringList) {
        this.stringList = stringList;
        this.valueType = ValueType.STRING_LIST;
        return this;
    }

    public FunctionValue ofFunction(Function fn) {
        function = fn;
        valueType = ValueType.FUNCTION;
        return this;
    }

    public FunctionValue ofInt(int value) {
        intValue = value;
        valueType = ValueType.INT;
        return this;
    }

    public FunctionValue ofObjectList(List<? extends Object> list) {
        objectList = list;
        valueType = ValueType.OBJECT_LIST;
        return this;
    }

    public FunctionValue ofObject(Object value) {
        if (value == null) {
            throw new FunctionValueException("null value not supported");
        }
        if (value instanceof String) {
            return ofString((String) value);
        }
        if (value instanceof Double) {
            return ofDouble((Double) value);
        }
        if (value instanceof Boolean) {
            return ofBoolean((Boolean) value);
        }
        if (value instanceof Integer) {
            return ofInt((Integer) value);
        }
        if (value instanceof Function) {
            return ofFunction((Function) value);
        }
        if (value instanceof List) {
            return ofObjectList((List<?>) value);
        }
        object = value;
        valueType = ValueType.OBJECT;
        return this;
    }

    public boolean isFunction() {
        return ValueType.FUNCTION.equals(valueType);
    }

    public Function asFunction() {
        return function;
    }

    public boolean isDoubleList() {
        return ValueType.DOUBLE_LIST.equals(valueType);
    }

    public List<Double> asDoubleList() {
        return doubleList;
    }

    public String asString() {
        if (isList()) {
            return listAsString();
        }
        switch (valueType) {
        case STRING:
            return stringValue;
        case DOUBLE:
            return Double.toString(doubleValue);
        case INT:
            return Integer.toString(intValue);
        case BOOLEAN:
            return Boolean.toString(booleanValue);
        case OBJECT:
            return String.valueOf(object);
        default:
            throw new FunctionValueException("Could not get string");
        }
    }

    private String listAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(Join.join(asList(), ", "));
        sb.append("]");
        return sb.toString();
    }

    public int asInt() {
        switch (valueType) {
        case STRING:
            double val = NumberUtils.parseInt(stringValue);
            return (int) val;
        case INT:
            return intValue;
        case DOUBLE:
            return (int) doubleValue;
        default:
            throw new FunctionValueException("Could not get int");
        }
    }

    public double asDouble() {
        switch (valueType) {
        case DOUBLE:
            return doubleValue;
        case STRING:
            return NumberUtils.parseDouble(stringValue);
        case INT:
            return (double) intValue;
        default:
            throw new FunctionValueException("Could not get double");
        }
    }

    public boolean asBoolean() {
        switch (valueType) {
        case BOOLEAN:
            return booleanValue;
        case STRING:
            return Boolean.parseBoolean(stringValue);
        case INT:
            return intValue == 1;
        case DOUBLE:
            return ((int) doubleValue) == 1;
        default:
            throw new FunctionValueException("Could not get boolean");
        }
    }

    public boolean isString() {
        return ValueType.STRING.equals(valueType);
    }

    public boolean isStringList() {
        return ValueType.STRING_LIST.equals(valueType);
    }

    public List<String> asStringList() {
        return stringList;
    }

    public boolean isList() {
        return ValueType.STRING_LIST.equals(valueType) || ValueType.DOUBLE_LIST.equals(valueType)
                || ValueType.OBJECT_LIST.equals(valueType);
    }

    public List<? extends Object> asList() {
        switch (valueType) {
        case STRING_LIST:
            return stringList;
        case DOUBLE_LIST:
            return doubleList;
        case OBJECT_LIST:
            return objectList;
        default:
            throw new FunctionValueException("Unsupported list type");
        }
    }

    public Object asObject() {
        switch (valueType) {
        case BOOLEAN:
            return booleanValue;
        case DOUBLE:
            return doubleValue;
        case INT:
            return intValue;
        case STRING:
            return stringValue;
        case DOUBLE_LIST:
            return doubleList;
        case OBJECT:
            return object;
        case FUNCTION:
            return function;
        case OBJECT_LIST:
            return objectList;
        case STRING_LIST:
            return stringList;
        default:
            return null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (booleanValue ? 1231 : 1237);
        result = prime * result + ((doubleList == null) ? 0 : doubleList.hashCode());
        long temp;
        temp = Double.doubleToLongBits(doubleValue);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((function == null) ? 0 : function.hashCode());
        result = prime * result + intValue;
        result = prime * result + ((object == null) ? 0 : object.hashCode());
        result = prime * result + ((objectList == null) ? 0 : objectList.hashCode());
        result = prime * result + ((stringList == null) ? 0 : stringList.hashCode());
        result = prime * result + ((stringValue == null) ? 0 : stringValue.hashCode());
        result = prime * result + ((valueType == null) ? 0 : valueType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FunctionValue other = (FunctionValue) obj;
        if (booleanValue != other.booleanValue)
            return false;
        if (doubleList == null) {
            if (other.doubleList != null)
                return false;
        } else if (!doubleList.equals(other.doubleList))
            return false;
        if (Double.doubleToLongBits(doubleValue) != Double.doubleToLongBits(other.doubleValue))
            return false;
        if (function == null) {
            if (other.function != null)
                return false;
        } else if (!function.equals(other.function))
            return false;
        if (intValue != other.intValue)
            return false;
        if (object == null) {
            if (other.object != null)
                return false;
        } else if (!object.equals(other.object))
            return false;
        if (objectList == null) {
            if (other.objectList != null)
                return false;
        } else if (!objectList.equals(other.objectList))
            return false;
        if (stringList == null) {
            if (other.stringList != null)
                return false;
        } else if (!stringList.equals(other.stringList))
            return false;
        if (stringValue == null) {
            if (other.stringValue != null)
                return false;
        } else if (!stringValue.equals(other.stringValue))
            return false;
        return valueType == other.valueType;
    }
}
