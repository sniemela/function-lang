/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.runtime;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.simo.function.integration.AllValuesPropertyHandler;
import org.simo.function.integration.AllowedValuesPropertyHandler;
import org.simo.function.nodes.FnCallable;
import org.simo.function.nodes.Function;
import org.simo.function.nodes.annotation.BuiltinFunction;
import org.simo.function.nodes.builtin.If;
import org.simo.function.nodes.builtin.Len;
import org.simo.function.nodes.builtin.arrays.Append;
import org.simo.function.nodes.builtin.arrays.Flatten;
import org.simo.function.nodes.builtin.arrays.Join;
import org.simo.function.nodes.builtin.math.Product;
import org.simo.function.nodes.builtin.math.Sqrt;
import org.simo.function.nodes.builtin.math.Sum;
import org.simo.function.nodes.builtin.math.ToDouble;
import org.simo.function.nodes.builtin.math.ToInt;
import org.simo.function.nodes.builtin.strings.Concat;

/**
 * @author sniemela
 *
 */
public final class FunctionRegistry {
    private static final Logger logger = Logger.getLogger(FunctionRegistry.class.getName());

    private final Map<String, Function> functions = new HashMap<>();

    public FunctionRegistry() {
        registerBuiltins();
    }

    private void registerBuiltins() {
        register(If.class);
        register(Len.class);
        register(Append.class);
        register(Flatten.class);
        register(Join.class);
        register(Product.class);
        register(Sqrt.class);
        register(Sum.class);
        register(ToDouble.class);
        register(ToInt.class);
        register(Concat.class);
        register(AllowedValuesPropertyHandler.class);
        register(AllValuesPropertyHandler.class);
    }

    public void register(Class<? extends FnCallable> klass) {
        BuiltinFunction spec = klass.getAnnotation(BuiltinFunction.class);
        String name = spec.name();
        if (name == null || name.trim().isEmpty()) {
            name = klass.getSimpleName().toLowerCase();
        }
        if (!FnCallable.class.isAssignableFrom(klass)) {
            logger.log(Level.WARNING, "Class does not implement FnCallable interface: {0}", klass);
        }
        try {
            Object newInstance = klass.getDeclaredConstructor().newInstance();
            Function fn = new Function(name, spec.minArgs(), spec.maxArgs());
            fn.setCallable((FnCallable) newInstance);
            functions.put(name, fn);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public Function lookup(String name) {
        return functions.get(name);
    }
}
