/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.integration;

import java.util.HashMap;
import java.util.Map;

import org.simo.function.integration.model.Attribute;
import org.simo.function.integration.model.Parameter;
import org.simo.function.integration.model.Value;
import org.simo.function.runtime.Environment;
import org.simo.function.runtime.ObjectCallSite;

/**
 * @author Simo
 *
 */
public final class DefaultObjectCallSite implements ObjectCallSite {

    private final Map<String, PropertyHandler> propertyHandlers = new HashMap<>();

    public void setPropertyHandler(String property, PropertyHandler handler) {
        propertyHandlers.put(property, handler);
    }

    public void removePropertyHandler(String property) {
        propertyHandlers.remove(property);
    }

    @Override
    public Object call(Object object, String property, Environment env) {
        Value value = null;
        if (object instanceof Value) {
            value = (Value) object;
        } else if (object instanceof Parameter) {
            value = ((Parameter) object).getSelectedValue();
        }
        if (value == null) {
            throw new IllegalArgumentException("Value is null in call site");
        }
        return callValueProperty(value, property);
    }

    private Object callValueProperty(Value value, String property) {
        PropertyHandler propertyHandler = propertyHandlers.get(property);

        if (propertyHandler != null) {
            return propertyHandler.eval(value.getParameter(), value, property);
        }

        Attribute attr = value.getAttributeByType(property);

        if (attr == null) {
            throw new IllegalArgumentException("Undefined value property: " + property);
        }

        return attr.getValue();
    }

    @Override
    public boolean canCall(Object obj, Object prop, Environment env) {
        if (obj == null) {
            return false;
        }
        return (obj instanceof Value) || (obj instanceof Parameter);
    }
}
