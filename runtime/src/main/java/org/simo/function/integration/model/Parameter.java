/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.integration.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author sniemela
 *
 */
public class Parameter {
    private List<Value> values;
    private Value selectedValue;
    private int id;

    public void setSelectedValue(Value value) {
        if (selectedValue != null) {
            selectedValue.setSelected(false);
        }
        selectedValue = value;
        if (selectedValue != null) {
            selectedValue.setSelected(true);
        }
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    public void addValue(Value value) {
        if (value == null) {
            throw new IllegalArgumentException("Cannot add null value");
        }
        if (values == null) {
            values = new ArrayList<>();
        }
        value.setParameter(this);
        values.add(value);
    }

    public List<Value> getValues() {
        if (values == null) {
            return Collections.emptyList();
        }
        return values;
    }

    public Value getSelectedValue() {
        return selectedValue;
    }

    public List<Value> getAllowedValues() {
        List<Value> allowedValues = new ArrayList<>(getValues().size());
        for (Value value : getValues()) {
            if (value.isAllowed()) {
                allowedValues.add(value);
            }
        }
        return allowedValues;
    }

    public List<Value> getConstraintAllowedValues() {
        List<Value> allowedValues = new ArrayList<>(getValues().size());
        for (Value value : getValues()) {
            if (value.isConAllowed()) {
                allowedValues.add(value);
            }
        }
        return allowedValues;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
