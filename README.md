# Function Language

This is a domain specific language (DSL) implementation designed for product configurators and other expert systems.
The goal of this DSL is to provide an easy and efficient way to do equations and 
other algorithms using Excel-like syntax to improve productivity on target systems.

This project contains an interpreter and a parser for the function language.
This project uses ANTLR to generate parser and lexer classes. The language and runtime module
are separated, so that ANTLR dependencies are not exposed to the runtime. The runtime module is dependency free. Depending on a use case, you could pre build AST model of the scripts 
and include the runtime module to execute those scripts.

## Requirements

Java 8+ (JDK)

## Running tests

In project root run:

```
./mvnw clean test
```

## Running benchmarks

In project root, run:

```
./mvnw clean package && java -jar benchmark/target/benchmarks.jar
```

## Examples

### Executing functions in Java program

```java
Node root = FunctionAST.parse("sum(1, 2, 3, 4)");
Environment env = Environment.create();
Object result = root.eval(env);
System.out.println(result);
```

### Quadratic equation

*Quadratic.fn*:
```
$x0 := -3 + sqrt(3^2 - 4*4)/2
$x1 := -3 - sqrt(3^2 - 4*4)/2
concat($x0, " ", $x1)
```

### Dot product of two equal-length vectors

*Vector.fn*:
```
$vector1 := [1,2,3,4,5]
$vector2 := [6,7,8,9,10]
$vector1 * $vector2
```

### Array access

*Array.fn*:
```
$array := [1,2,3,4,5,6]
$array[1] # This prints "2"
```

A sub list access is also supported.

*ArraySubList.fn*:
```
$array := [1,2,3,4,7,8]
$array[1:5] # This prints "[2,3,4,7]"
```

### Filtering items

*Filtering.fn*:
```
$items := [1,2,3,4,5,6,7,8,9,10]

# Lets select items that are divisible by 2
$result := $items as number where number % 2 == 0

$result # This prints "[2, 4, 6, 8, 10]"
```

## Benchmarks

Performance of the function language is compared to XSLT scripts and Java (baseline). 
XSLT scripts are often used in expert systems to do calculations and data manipulation so it's interesting 
to see how function-lang performs in same benchmark tests.

```
Run complete. Total time: 00:02:03

Benchmark                                                  Mode  Cnt        Score       Error  Units
QuadraticEquationBenchmark.quadraticEquationFunctionLang  thrpt   10  1227949,969 ±  4386,289  ops/s
QuadraticEquationBenchmark.quadraticEquationJava          thrpt   10  8138906,043 ± 20224,225  ops/s
QuadraticEquationBenchmark.quadraticEquationXSLT          thrpt   10     6760,233 ±    45,902  ops/s
SumOfDivisibleByTwoBenchmark.whereExpressionFunctionLang  thrpt   10    62576,307 ±   267,604  ops/s
SumOfDivisibleByTwoBenchmark.whereExpressionJava          thrpt   10   194965,561 ±   518,368  ops/s
SumOfDivisibleByTwoBenchmark.whereExpressionXslt          thrpt   10      101,569 ±     0,724  ops/s
```