/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.simo.function.nodes.Node;
import org.simo.function.nodes.RootNode;
import org.simo.function.nodes.expression.ArrayAccessNode;
import org.simo.function.nodes.expression.ArrayNode;
import org.simo.function.nodes.expression.DirectFunctionCallNode;
import org.simo.function.nodes.expression.IndirectFunctionCallNode;
import org.simo.function.nodes.expression.LiteralNode;
import org.simo.function.nodes.expression.PropertyAccessNode;
import org.simo.function.nodes.expression.SymbolNode;
import org.simo.function.nodes.expression.VarNode;
import org.simo.function.nodes.expression.WhereNode;
import org.simo.function.nodes.expression.arithm.AddNode;
import org.simo.function.nodes.expression.arithm.DivNode;
import org.simo.function.nodes.expression.arithm.ExpNode;
import org.simo.function.nodes.expression.arithm.ModNode;
import org.simo.function.nodes.expression.arithm.MulNode;
import org.simo.function.nodes.expression.arithm.SubNode;
import org.simo.function.nodes.expression.logic.AndNode;
import org.simo.function.nodes.expression.logic.EqNode;
import org.simo.function.nodes.expression.logic.GeNode;
import org.simo.function.nodes.expression.logic.GtNode;
import org.simo.function.nodes.expression.logic.LeNode;
import org.simo.function.nodes.expression.logic.LtNode;
import org.simo.function.nodes.expression.logic.NeNode;
import org.simo.function.nodes.expression.logic.NegateNode;
import org.simo.function.nodes.expression.logic.NotNode;
import org.simo.function.nodes.expression.logic.OrNode;
import org.simo.function.nodes.expression.logic.XorNode;
import org.simo.function.nodes.statement.AssignmentNode;
import org.simo.function.parser.FunctionParser.ArgumentListContext;
import org.simo.function.parser.FunctionParser.ArrayAccessExprContext;
import org.simo.function.parser.FunctionParser.ArrayLiteralContext;
import org.simo.function.parser.FunctionParser.ArrayLiteralExprContext;
import org.simo.function.parser.FunctionParser.AssignmentContext;
import org.simo.function.parser.FunctionParser.BinaryExprContext;
import org.simo.function.parser.FunctionParser.ExpressionContext;
import org.simo.function.parser.FunctionParser.FunctionExprContext;
import org.simo.function.parser.FunctionParser.GroupExprContext;
import org.simo.function.parser.FunctionParser.LiteralContext;
import org.simo.function.parser.FunctionParser.LiteralExprContext;
import org.simo.function.parser.FunctionParser.LogicExprContext;
import org.simo.function.parser.FunctionParser.MemberAccessExprContext;
import org.simo.function.parser.FunctionParser.PrimaryContext;
import org.simo.function.parser.FunctionParser.ProgramContext;
import org.simo.function.parser.FunctionParser.UnaryContext;
import org.simo.function.parser.FunctionParser.UnaryExprContext;
import org.simo.function.parser.FunctionParser.WhereExprContext;

/**
 * @author Simo
 *
 */
public final class FunctionAST extends FunctionBaseVisitor<Node> {

    private static final Node[] EMPTY_ARGS = new Node[0];

    public static Node parse(String code) {
        return parse(CharStreams.fromString(code));
    }

    public static final Node parse(InputStream inputStream) throws IOException {
        return parse(CharStreams.fromStream(inputStream));
    }

    private static final Node parse(CharStream stream) {
        FunctionLexer lexer = new FunctionLexer(stream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FunctionParser parser = new FunctionParser(tokens);
        FunctionAST gen = new FunctionAST();
        return gen.visit(parser.program());
    }

    @Override
    public Node visitProgram(ProgramContext ctx) {
        return new RootNode(ctx.statement().stream().map(this::visit).toArray(Node[]::new));
    }

    @Override
    public Node visitAssignment(AssignmentContext ctx) {
        String varName = ctx.VAR().getText();
        Node expr = visit(ctx.expression());
        return new AssignmentNode(varName, expr);
    }

    @Override
    public Node visitMemberAccessExpr(MemberAccessExprContext ctx) {
        Node obj = visit(ctx.obj);
        return new PropertyAccessNode(obj, ctx.prop.getText());
    }

    @Override
    public Node visitFunctionExpr(FunctionExprContext ctx) {
        Node callee = visit(ctx.expression());
        List<ArgumentListContext> argumentList = ctx.argumentList();

        if (!argumentList.isEmpty()) {
            for (ArgumentListContext argContext : ctx.argumentList()) {
                Node[] args = argContext.expression().stream().map(this::visit).toArray(Node[]::new);
                callee = createFunctionCallNode(callee, args);
            }
        } else {
            callee = createFunctionCallNode(callee, EMPTY_ARGS);
        }

        return callee;
    }

    private Node createFunctionCallNode(Node callee, Node[] args) {
        if (callee instanceof SymbolNode) {
            String functionName = ((SymbolNode) callee).getId();
            return new DirectFunctionCallNode(functionName, args);
        } else {
            return new IndirectFunctionCallNode(callee, args);
        }
    }

    @Override
    public Node visitGroupExpr(GroupExprContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public Node visitLiteralExpr(LiteralExprContext ctx) {
        return visitLiteral(ctx.literal());
    }

    @Override
    public Node visitLogicExpr(LogicExprContext ctx) {
        Node left = visit(ctx.left);
        Node right = visit(ctx.right);
        switch (ctx.op.getType()) {
        case FunctionLexer.AND:
            return new AndNode(left, right);
        case FunctionLexer.OR:
            return new OrNode(left, right);
        case FunctionLexer.XOR:
            return new XorNode(left, right);
        default:
            throw new IllegalStateException("Invalid logic expression: " + ctx.getText());
        }
    }

    @Override
    public Node visitBinaryExpr(BinaryExprContext ctx) {
        Node left = visit(ctx.left);
        Node right = visit(ctx.right);
        switch (ctx.op.getType()) {
        case FunctionLexer.ADD:
            return new AddNode(left, right);
        case FunctionLexer.MULT:
            return new MulNode(left, right);
        case FunctionLexer.DIV:
            return new DivNode(left, right);
        case FunctionLexer.MOD:
            return new ModNode(left, right);
        case FunctionLexer.SUB:
            return new SubNode(left, right);
        case FunctionLexer.EXP:
            return new ExpNode(left, right);
        case FunctionLexer.LE:
            return new LeNode(left, right);
        case FunctionLexer.LT:
            return new LtNode(left, right);
        case FunctionLexer.GE:
            return new GeNode(left, right);
        case FunctionLexer.GT:
            return new GtNode(left, right);
        case FunctionLexer.NE:
            return new NeNode(left, right);
        case FunctionLexer.EQ:
            return new EqNode(left, right);
        default:
            throw new IllegalStateException("Illegal binary expr");
        }
    }

    @Override
    public Node visitUnaryExpr(UnaryExprContext ctx) {
        return visit(ctx.unary());
    }

    @Override
    public Node visitUnary(UnaryContext ctx) {
        Node expr = visit(ctx.primary());
        switch (ctx.op.getType()) {
        case FunctionLexer.NOT:
        case FunctionLexer.BANG:
            return new NotNode(expr);
        case FunctionLexer.SUB:
            return new NegateNode(expr);
        default:
            throw new IllegalStateException("Invalid unary operation: " + ctx.op.getText());
        }
    }

    @Override
    public Node visitPrimary(PrimaryContext ctx) {
        LiteralContext literal = ctx.literal();
        ExpressionContext expression = ctx.expression();
        if (literal != null) {
            return visitLiteral(literal);
        } else if (expression != null) {
            return visit(expression);
        }
        throw new IllegalStateException("Primary null");
    }

    @Override
    public Node visitWhereExpr(WhereExprContext ctx) {
        Node list = visit(ctx.list);
        Node conditions = visit(ctx.cond);

        String localVar = null;
        if (ctx.as != null) {
            localVar = ctx.as.getText();
        }

        Node select = null;
        if (ctx.select != null) {
            select = visit(ctx.select);
        }

        Node limit = null;
        if (ctx.limit != null) {
            limit = visit(ctx.limit);
        }

        return new WhereNode(list, localVar, conditions, select, limit);
    }

    @Override
    public Node visitArrayLiteralExpr(ArrayLiteralExprContext ctx) {
        return visitArrayLiteral(ctx.arrayLiteral());
    }

    @Override
    public Node visitArrayAccessExpr(ArrayAccessExprContext ctx) {
        Node object = visit(ctx.obj);
        Node firstValue = visit(ctx.first);
        Node lastValue = null;

        if (ctx.last != null) {
            lastValue = visit(ctx.last);
        }

        return new ArrayAccessNode(object, firstValue, lastValue);
    }

    @Override
    public Node visitArrayLiteral(ArrayLiteralContext ctx) {
        ArgumentListContext listContext = ctx.argumentList();
        return new ArrayNode(listContext.expression().stream().map(this::visit).toArray(Node[]::new));
    }

    @Override
    public Node visitLiteral(LiteralContext ctx) {
        switch (ctx.type.getType()) {
        case FunctionLexer.NUMBER:
            return new LiteralNode(Double.parseDouble(ctx.getText()));
        case FunctionLexer.STRING:
            String v = ctx.getText();
            return new LiteralNode(v.substring(1, v.length() - 1));
        case FunctionLexer.NIL:
            return new LiteralNode(null);
        case FunctionLexer.BOOLEAN:
            return new LiteralNode(Boolean.parseBoolean(ctx.getText()));
        case FunctionLexer.VAR:
            return new VarNode(ctx.getText());
        case FunctionLexer.ID:
            return new SymbolNode(ctx.getText());
        default:
            throw new IllegalStateException("Unsupported literal");
        }
    }
}
