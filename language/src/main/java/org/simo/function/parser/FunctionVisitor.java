// Generated from Function.g4 by ANTLR 4.7
package org.simo.function.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FunctionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 *        operations with no return type.
 */
public interface FunctionVisitor<T> extends ParseTreeVisitor<T> {
    /**
     * Visit a parse tree produced by {@link FunctionParser#program}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitProgram(FunctionParser.ProgramContext ctx);

    /**
     * Visit a parse tree produced by {@link FunctionParser#statement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStatement(FunctionParser.StatementContext ctx);

    /**
     * Visit a parse tree produced by {@link FunctionParser#assignment}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignment(FunctionParser.AssignmentContext ctx);

    /**
     * Visit a parse tree produced by the {@code groupExpr} labeled alternative in
     * {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitGroupExpr(FunctionParser.GroupExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code unaryExpr} labeled alternative in
     * {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUnaryExpr(FunctionParser.UnaryExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code arrayAccessExpr} labeled
     * alternative in {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrayAccessExpr(FunctionParser.ArrayAccessExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code whereExpr} labeled alternative in
     * {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitWhereExpr(FunctionParser.WhereExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code literalExpr} labeled alternative in
     * {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLiteralExpr(FunctionParser.LiteralExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code logicExpr} labeled alternative in
     * {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLogicExpr(FunctionParser.LogicExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code memberAccessExpr} labeled
     * alternative in {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMemberAccessExpr(FunctionParser.MemberAccessExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code binaryExpr} labeled alternative in
     * {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBinaryExpr(FunctionParser.BinaryExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code arrayLiteralExpr} labeled
     * alternative in {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrayLiteralExpr(FunctionParser.ArrayLiteralExprContext ctx);

    /**
     * Visit a parse tree produced by the {@code functionExpr} labeled alternative
     * in {@link FunctionParser#expression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunctionExpr(FunctionParser.FunctionExprContext ctx);

    /**
     * Visit a parse tree produced by {@link FunctionParser#arrayLiteral}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrayLiteral(FunctionParser.ArrayLiteralContext ctx);

    /**
     * Visit a parse tree produced by {@link FunctionParser#unary}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUnary(FunctionParser.UnaryContext ctx);

    /**
     * Visit a parse tree produced by {@link FunctionParser#primary}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPrimary(FunctionParser.PrimaryContext ctx);

    /**
     * Visit a parse tree produced by {@link FunctionParser#argumentList}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArgumentList(FunctionParser.ArgumentListContext ctx);

    /**
     * Visit a parse tree produced by {@link FunctionParser#literal}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLiteral(FunctionParser.LiteralContext ctx);
}