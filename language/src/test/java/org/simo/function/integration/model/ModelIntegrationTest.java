/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.integration.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.simo.function.TestBase;

public class ModelIntegrationTest extends TestBase {

    @Test
    public void allValues() {
        Parameter parameter = new Parameter();
        Value value = new Value();
        parameter.addValue(value);
        Attribute attr = new Attribute("attr", "1");
        value.addAttribute(attr);

        env.defineSymbol("parameter", parameter);

        Object result = run(
                "$allValues := [allvalues]\nsum($allValues as fn where double(fn(parameter)[0].attr) >= 0 select fn(parameter)[0].attr)");
        assertEquals("1.0", result);
    }
}
