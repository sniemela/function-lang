/*
 * Copyright (c) 2018 Simo Niemelä
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.simo.function.nodes.expression;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.simo.function.TestBase;

/**
 * @author Simo
 *
 */
public class ArrayTest extends TestBase {

    @Test
    public void arrayAccess() {
        Object result = run("int(if(1==1,[1,2],[10,20])[0])");
        assertEquals("1", result);
    }

    @Test
    public void lengthOnArray() {
        Object result = run("int(len([1,2,3,4,5]))");
        assertEquals("5", result);
    }

    @Test
    public void charAccessOnString() {
        Object result = run("\"Simo\"[2]");
        assertEquals("m", result);
    }

    @Test
    public void testLiteralArray() {
        Object result = run("[1,2,3]");
        assertEquals(toStringList("1.0", "2.0", "3.0"), result);
    }

    @Test
    public void vectorMultiplication() {
        Object result = run("[1,2] * [1,2]");
        assertEquals("5.0", result);
    }

    @Test
    public void testSubArrayAccess() {
        Object result = run("[1,2,3,4,5,6,7,8,9,10][3:6]");
        assertEquals(toStringList("4.0", "5.0", "6.0"), result);
    }
}
